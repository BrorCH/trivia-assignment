import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import GameOver from './views/GameOver.vue'
import GamePlay from './views/GamePlay.vue'
import GameMenu from './views/GameMenu.vue'

Vue.use(VueRouter);

Vue.config.productionTip = false




const routes = [
  {
    path: '/GameOver',
    component: GameOver,
    name: 'GameOver'
  },
  {
    path: '/GamePlay',
    component: GamePlay
  },
  {
    path: '/',
    component: GameMenu
  }
];

const router = new VueRouter({
  routes
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
